'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3977;

const uri = 'mongodb://localhost:27017/curso_mean2';

const options = {
    autoIndex: false, // Don't build indexes
    maxPoolSize: 10, // Maintain up to 10 socket connections
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
  };

mongoose.connect(uri, options).then(
    () => {console.log("conexión realizada a mongo...");
          app.listen(port, function(){
            console.log("Servidor del API Rest de música escuchando en Http://localhost:"||port);
          })
        },
    err => {console.log(err)}
);