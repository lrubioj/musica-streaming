'use strict'

let User = require('../models/user');
let bcrypt = require('bcryptjs');
let jwt = require('../services/jwt');
let fs = require('fs');
let path = require('path');

const SALT_WORK_FACTORY = 10;

function pruebas(req, res){
    res.status(200).send({
        message: 'Probando acción del controlador de usuarios del API REST Node'
    });
}

function saveUser(req, res){
    let user = new User();

    let params = req.body;

    console.log(req.body);
    console.log('params.password: '+params.password);

    user.name = params.name;
    user.surname = params.surname;
    user.email = params.email;
    user.role = 'ROLE_ADMIN';
    user.image = 'null';

    if(params.password){
        //encriptar contraseña y guardar datos
        bcrypt.genSalt(SALT_WORK_FACTORY, function (err, salt) {
            bcrypt.hash(params.password, salt, function(err, hash){
                user.password = hash;
                console.log('user.password: '+user.password);
                if(user.name != null && user.surname != null && user.email != null){
                    //guardar usuario
                    user.save((err, userStored) => {
                        if(err){
                            res.status(500).send({
                                message: 'Error al guardar el usuario'
                            }); 
                        }else{
                            if(!userStored){
                                res.status(404).send({
                                    message: 'No se ha registrado el usuario'
                                }); 
                            }else{
                                res.status(200).send({
                                    user: userStored
                                })
                            }
                        }
                    });
                }else{
                    res.status(200).send({
                        message: 'Rellena todos los campos'
                    });
                }
            });
        });
    }else{
        res.status(200).send({
            message: 'Introduce la contraseña'
        });
    }
}

function loginUser(req, res){

    let params = req.body;
    let email = params.email;
    let password = params.password;

    User.findOne({email: email.toLowerCase()}, (err, user)=>{
        if(err){
            res.status(500).send({
                message: 'Error en la petición'
            });
        }else{
            if(!user){
                res.status(404).send({
                    message: 'El usuario no existe'
                });
            }else{
                //comprobar la contraseña
                bcrypt.compare(password, user.password, (err, check)=>{
                    if(check){
                        //devolver los datos del usuario logueado
                        if(params.gethash){
                            //devolver un toquen de jwt
                            res.status(200).send({
                                token: jwt.createToken(user)
                            })
                        }else{
                            res.status(200).send({
                                user: user
                            })
                        }
                    }else{
                        res.status(404).send({
                            message: 'el usuario no ha podido loguearse'
                        });
                    }
                })
            }
        }
    })
}

function updateUser(req, res){
    let userId = req.params.id;
    let update = req.body;

    User.findByIdAndUpdate(userId, update, (err, userUpdated) => {
        if(err){
            res.status(500).send({
                message: 'Error al actualizar el usuario.'
            });
        }else{
            if(!userUpdated){
                res.status(404).send({
                    message: 'No se ha podido actualizar el usuario.'
                });
            }else{
                res.status(200).send({
                    user: userUpdated
                });
            }
        }
    });
}

function uploadImages(req, res){
    let userId = req.params.id;
    let fileName = 'No subir...';

    if(req.files){
        let file_path = req.files.image.path;
        let file_split = file_path.split('\\');
        let file_name = file_split[2];
        let ext_split = file_name.split('\.');
        var file_ext = ext_split[1];

        if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif'){
            User.findByIdAndUpdate(userId, {image: file_name}, (err, userUpdated) => {
                if(err){
                    res.status(500).send({
                        message: 'Error al actualizar la imagen del usuario.'
                    });
                }else{
                    if(!userUpdated){
                        res.status(404).send({
                            message: 'No se ha podido actualizar la imagen.'
                        });
                    }else{
                        res.status(200).send({
                            user: userUpdated
                        });
                    }
                }
            })
        }else{
            res.status(200).send({
                message: 'Extenció n no válida'
            });
        }

        console.log(file_split);
    }else{
        res.status(200).send({
            message: 'No has subido ninguna imagen...'
        });
    }
}

function getImageFile(req, res){
    let imageFile = req.params.imageFile;
    let pathfile = './uploads/users/'+imageFile;

    let existsFile = fs.existsSync(pathfile);

    if(existsFile){
        res.sendFile(path.resolve(pathfile));
    }else{
        res.status(200).send({
            message: 'No existe la imagen.'
        });
    }
}

module.exports = {
    pruebas,
    saveUser,
    loginUser,
    updateUser,
    uploadImages,
    getImageFile
};