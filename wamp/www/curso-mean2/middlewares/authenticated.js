'use strict'

let jwt = require('jwt-simple');
let moment = require('moment');
const SECRET = 'cl@ve_$eCr4t@';
let payload;

exports.ensureAuth = (req, res, next) => {
    if(!req.headers.authorization){
        return res.status(403).send({
            message: 'La petición no tiene la cabecera de autenticación'
        });
    }
    let token = req.headers.authorization.replace(/['"]+/g, '');
    
    try{
        payload = jwt.decode(token, SECRET);
        let expiration = payload.exp;
        if(expiration <= moment().unix()){
            
            return res.status(401).send({
                message: 'Token ha expirado'
            });
        }
    }catch(ex){
        console.log(ex);
        return res.status(403).send({
            message: 'Token no válido'
        });
    }

    req.user = payload;

    next();
}