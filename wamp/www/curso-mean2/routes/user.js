'use strict'

let express = require('express');
let Usercontroller = require('../controllers/user');
let md_auth = require('../middlewares/authenticated');
let api = express.Router();
let multipart = require('connect-multiparty');

let md_upload = multipart({uploadDir: './uploads/users'})

api.get('/probando-controlador', md_auth.ensureAuth, Usercontroller.pruebas);
api.post('/register', Usercontroller.saveUser);
api.get('/login', Usercontroller.loginUser);
api.put('/update-user/:id',md_auth.ensureAuth, Usercontroller.updateUser);
api.post('/upload-image-user/:id', [md_auth.ensureAuth, md_upload], Usercontroller.uploadImages);
api.get('/get-image-user/:imageFile', Usercontroller.getImageFile);
module.exports = api;