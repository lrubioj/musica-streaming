'use strict'

let jwt = require('jwt-simple');
let moment = require('moment');
const SECRET = 'cl@ve_$eCr4t@';

exports.createToken = (user) => {
    let payload = {
        sub: user._id, 
        name: user.name,
        surname: user.surname,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment().unix(),
        exp: moment().add(30, 'd').unix()
    };

    return jwt.encode(payload, SECRET);
};